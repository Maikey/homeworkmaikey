package com.epam.uni.pagemodels.pagesAtClass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.epam.uni.entities.LoginModel;

public class LoginPage extends BasePage {
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	// TASK 1.2: implement create method in LoginPage, use PageFactory to initialization
	public static LoginPage create(WebDriver driver) {
		//return null;
		return PageFactory.initElements(driver, LoginPage.class);
	}

	@FindBy(how = How.NAME, using = "username")
	public WebElement usernameField;

	@FindBy(how = How.NAME, using = "password")
	private WebElement passwordField;
	
	@FindBy(how = How.ID, using = "login-button")
	private WebElement loginButton;
	
	// TASK 2.1: implement fillLoginForm method
	public void fillLoginForm(LoginModel model)
	{
		// TASK 2.2: implement setUsername
		setUsername(model.getUsername());
		// TASK 2.3: implement setPassword
		setPassword(model.getPassword());
		// TASK 2.4: implement clickLoginButton
		clickLoginButton();
	}

	private void setUsername(String value) {

		if (value != null)
		{
			usernameField.sendKeys(value);
		}
	}
	
	private void setPassword(String value) {

		if (value != null)
		{
			passwordField.sendKeys(value);
		}
	}	
	
	private void clickLoginButton() {

		loginButton.click();
	}

}
