package com.epam.uni.testsAtClass;


import java.io.File;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;


public class Homework extends TestBase {
	//public static void main(String [] args) {
//	private final String Trello_URL = "https://trello.com/login";
	
	public Homework() {
		super();
	}
	
	@Test
    public void loginShouldWorkTest() throws Exception
    {
		//driver.get(Trello_URL);
		driver.navigate().to("https://trello.com/login"); 


		driver.findElement(By.cssSelector("#user")).sendKeys("pazmany.webdriver@gmail.com");
	    driver.findElement(By.cssSelector("#password")).sendKeys("WebDriver");
		
		//Log In(I couldn't use this because the website is in hungarian I tried english but still had errors)
		//WebDriverWait wait = new WebDriverWait(driver, 5);//
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Log In")));
		//for login I used this
        
        driver.findElement(By.cssSelector("#login")).click();
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        
        //https://www.guru99.com/take-screenshot-selenium-webdriver.html
        Homework.takeSnapShot(driver, "./TrelloLogin.png") ;

        //open my  my board <Maikey>
        // Open your own table which was named after you (<Firstname>_<Lastname>)
        driver.findElement(By.cssSelector("#content > div > div.js-boards-page > div > div > div > div > div.all-boards > div > div > div.boards-page-board-section.mod-no-sidebar > ul > li:nth-child(1) > a > div")).click();
        Homework.takeSnapShot(driver, "./Trellomyboard.png") ;
        //click on new card
        //FH 2018-11-26 11:42
        // In the ’Todo’ list of your own table create a new card as per the followings:
        //o Your monogram + the actual timestamp (e.g. FH 2018-11-26 11:42)
        
        //click on new card
        driver.findElement(By.cssSelector("#board > div:nth-child(1) > div.list.js-list-content > a")).click();
        // add time to the card
        driver.findElement(By.cssSelector("#board > div:nth-child(1) > div.list.js-list-content > div.list-cards.u-fancy-scrollbar.u-clearfix.js-list-cards.js-sortable.ui-sortable > div > div.list-card.js-composer > div > textarea")).sendKeys("MK:2018-11-26 11:42");
        Homework.takeSnapShot(driver, "./TrelloCardTime.png") ;
        
        //click on the add card button   
        driver.findElement(By.cssSelector("#board > div:nth-child(1) > div.list.js-list-content > div.list-cards.u-fancy-scrollbar.u-clearfix.js-list-cards.js-sortable.ui-sortable > div > div.cc-controls.u-clearfix > div:nth-child(1) > input")).click();
        
        Homework.takeSnapShot(driver, "./TrelloNewCard.png") ;
        
        //click on card
        driver.findElement(By.cssSelector("#board > div:nth-child(1) > div.list.js-list-content > div.list-cards.u-fancy-scrollbar.u-clearfix.js-list-cards.js-sortable.ui-sortable > a:nth-child(1) > div.list-card-details.js-card-details")).click();								
        
        Homework.takeSnapShot(driver, "./TrelloClickCard.png") ;
        //click on move
        driver.findElement(By.cssSelector("#classic > div.window-overlay > div > div > div > div.window-sidebar > div:nth-child(3) > div > a.button-link.js-move-card")).click();
        
        //select drop down list
        driver.findElement(By.cssSelector("#classic > div.pop-over.is-shown > div > div:nth-child(2) > div > div > div > div:nth-child(2) > div.button-link.setting.form-grid-child.form-grid-child-threequarters > select")).click();
        
        Homework.takeSnapShot(driver, "./TrelloDropDown.png") ;
        
        //select Done list
       // driver.findElement(By.linkList
        ////////////////////
        
        Select dropdown = new Select(driver.findElement(By.className("js-select-list")));
		dropdown.selectByVisibleText("Done");
		
        ///////////////////////
        
        //click on move
        driver.findElement(By.cssSelector("#classic > div.pop-over.is-shown > div > div:nth-child(2) > div > div > div > input")).click();

        Homework.takeSnapShot(driver, "./TrelloMovingCard.png") ;
        
        String CardTrueAssert = driver.findElement(By.cssSelector("a[class='js-open-move-from-header']")).getText();
		Assert.assertTrue(CardTrueAssert.equals("Done"));
		
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
}
	
	public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{

        //Convert web driver object to TakeScreenshot

        TakesScreenshot scrShot =((TakesScreenshot)webdriver);

        //Call getScreenshotAs method to create image file

                File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

            //Move image file to new destination

                File DestFile=new File(fileWithPath);

                //Copy file at destination

                FileUtils.copyFile(SrcFile, DestFile);

    }
}



