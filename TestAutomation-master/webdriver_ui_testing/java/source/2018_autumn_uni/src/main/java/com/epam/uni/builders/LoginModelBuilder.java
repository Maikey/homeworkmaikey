package com.epam.uni.builders;

import com.epam.uni.entities.LoginModel;

public class LoginModelBuilder {

	private String username;
	private String password;
	
	public static LoginModelBuilder simpleBuilder()
	{
		LoginModelBuilder builder = new LoginModelBuilder();
		builder.username = "admin";
		builder.password = "admin";
		return builder;
	}
	
	public LoginModel build()
	{
		LoginModel model = new LoginModel();
		model.setUsername(username);
		model.setPassword(password);
		return model;
	}
}
