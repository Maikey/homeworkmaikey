# TestAutomation

Please add a new directory for each class specific material (e.g. 01_UT, 12_PT).


# Software requirements
- Git: Git-2.18.0-64-bit (https://git-scm.com/download/win)
- Java: jdk-8u181-windows-x64.exe (http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/jdk-8u181-windows-x64.exe) 
- Apache Maven: apache-maven-3.5.4 (http://xenia.sote.hu/ftp/mirrors/www.apache.org/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.zip) 
- Eclipse 4.8 (http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/photon/R/eclipse-jee-photon-R-win32-x86_64.zip&mirror_id=1190)
- Apache Tomcat 9.0.11 (http://xenia.sote.hu/ftp/mirrors/www.apache.org/tomcat/tomcat-9/v9.0.11/bin/apache-tomcat-9.0.11-windows-x64.zip)